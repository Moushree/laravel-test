<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function fetchData($id)
    {
        $order = DB::table('orders')->select('orders.*')->where('orderNumber','=',$id)->get();

        $jsonresult = array();
       
        $jsonresult['order_id']=$order[0]->orderNumber;
        $jsonresult['order_date']=$order[0]->orderDate;
        $jsonresult['status']=$order[0]->status;
        for($i=0 ;$i<count($order);$i++)
        {
            $id = $order[$i]->orderNumber;
            $jsonresult['order_details']=DB::table('orderdetails')->join('products','orderdetails.productCode','=','products.productCode')->select(DB::raw("products.productName as product, products.productLine as product_line, orderdetails.priceEach as unit_price, orderdetails.quantityOrdered as qty, (orderdetails.priceEach * orderdetails.quantityOrdered) as line_total"))->where('orderNumber','=',$id)->get();
           $bm=DB::table("orderdetails")->join("products",'orderdetails.productCode','=','products.productCode')->select(DB::raw("SUM(orderdetails.priceEach * orderdetails.quantityOrdered) as bill_amount"))->where('orderNumber','=',$id)->first();
           $jsonresult['bill_amount']=$bm->bill_amount;
           
        }
        $custId = $order[0]->customerNumber;
        $jsonresult['customer']=DB::table("customers")->select('contactFirstName as first_name','contactLastName as last_name','phone','country as country_code')->where('customerNumber','=',$custId)->first();

        return response()->json( $jsonresult, 200);
        //return DB::select('select * from orders');
    }
}
